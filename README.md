# vuexlearn

手摸手，带你用 vue 动画实现原生 app 切换效果，丝滑般的体验


> vuex 学习项目，博客地址：http://stackfing.com/2018/05/22/vuexlearn/

效果图

![效果](http://upload-images.jianshu.io/upload_images/4438070-f40294da0943c89b?imageMogr2/auto-orient/strip)

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
